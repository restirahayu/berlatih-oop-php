<?php
require_once ('animal.php');
require_once ('frog.php');
require_once ('ape.php');

$sheep = new animal("shaun");

echo "<h3>Release 0</h3>";
echo $sheep->name.'<br>';
echo $sheep->legs.'<br>';
echo $sheep->cold_blooded.'<br>';

echo "<h3>Release 1</h3>";
$kodok = new frog("buduk");

echo $kodok->name.'<br>';
echo $kodok->legs.'<br>';
$kodok->jump();
echo "<br><br>";

$sungokong= new ape("kera sakti");
echo $sungokong->name.'<br>';
$sungokong->yell();


?>